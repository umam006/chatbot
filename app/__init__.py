import eventlet
eventlet.monkey_patch(thread=True, time=True)

import os
from flask import Flask
from flask_socketio import SocketIO

app = Flask(__name__)
app.config.from_pyfile('config.py')
app.config['SECRET_KEY'] = app.config.get("APP_SECRET_KEY")
socketio = SocketIO(app, cors_allowed_origins=app.config.get("APP_ORIGIN"))

from app import socket_routes
from app import routes
from app.services.telegram_service import telegrambot