import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

APP_SECRET_KEY = os.environ.get('APP_SECRET_KEY')
APP_ORIGIN = os.environ.get('APP_ORIGIN')
APP_ENV = os.environ.get('APP_ENV')
TELEGRAM_BOT_TOKEN = os.environ.get('TELEGRAM_BOT_TOKEN')