import pandas as pd
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
import string

factoryStopWord = StopWordRemoverFactory()
factoryStemmer = StemmerFactory()
stopword = factoryStopWord.create_stop_word_remover()
stemmer = factoryStemmer.create_stemmer()

class TextHelper:
    def cleanText(self, text):
        slang = pd.read_excel('app/static/slang.xlsx', header=0)
        slang = slang.values.tolist()
        text = self.cleaning(text)
        text = text.lower()
        text = self.convert_slang(text, slang)
        text = stopword.remove(text)
        text = self.steaming(text)
        return text
    
    def cleaning(self, text):
        for prohibited_symbol in string.punctuation:
            text = text.replace(prohibited_symbol, ' ')
        text = ' '.join(text.split())
        return text.strip(' ')

    def steaming(self, text):
        return stemmer.stem(text)

    def convert_slang(self, text, slang):
        for x in slang:
            text = text.replace(x[0], x[1])
        return text