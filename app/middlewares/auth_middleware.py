import functools
from flask_socketio import disconnect
from flask import request, session

def auth_socketio(f):
    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        if request.args.get('token') != 'jidajsdj320e03dj239d329d902dksad0dsa2da3j':
            disconnect()
        else:
            return f(*args, **kwargs)
    return wrapped