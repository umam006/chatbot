import pandas as pd

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import confusion_matrix 
from sklearn.metrics import accuracy_score 
from sklearn.metrics import classification_report
from app.helpers.text_helper import TextHelper

vectorizer = TfidfVectorizer()

class ChatbotService:
    def getAnswer(self, question):
        texthelper = TextHelper()
        question = [texthelper.cleanText(question)]
        questionTFIDF = vectorizer.fit_transform(question)
        QAset = pd.read_excel('app/static/qa-set.xlsx', header=0)
        XTFIDF = vectorizer.transform(QAset.question)
        vals = cosine_similarity(questionTFIDF, XTFIDF)
        big = 0
        for x in range(0,len(vals[0])):
            if vals[0][big] < vals[0][x]:
                big = x
        flat = vals.flatten()
        flat.sort()
        req_tfidf = flat[-1]

        if(req_tfidf<0.6):
            answer = 'maaf aku tidak paham dengan pertanyaan kamu!'
        else:
            answer = QAset.answer[big]
        return answer