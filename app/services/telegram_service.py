import telebot
from app import app
from app.services.chatbot_service import ChatbotService
from apscheduler.schedulers.background import BackgroundScheduler
import datetime
import time

clients = []

telegrambot = telebot.TeleBot(app.config.get("TELEGRAM_BOT_TOKEN"))
@telegrambot.message_handler(commands=['start'])
def start(message):
    client = next((item for item in clients if item['id'] == message.chat.id), False)
    if client == False:
        clients.append({'id':message.chat.id, 'time': datetime.datetime.now()})
        telegrambot.send_message(message.chat.id, 'Halo :)')
        telegrambot.send_message(message.chat.id, 'Aku Chat Bot, asisten virtual kamu. ada yang bisa aku bantu. ?')
    else:
        telegrambot.send_message(message.chat.id, 'kamu sudah masuk ke sesi chat')
@telegrambot.message_handler(func=lambda message: True)
def ask_question(message):
    client = next((item for item in clients if item['id'] == message.chat.id), False)
    if client != False:
        client['time'] = datetime.datetime.now()
        question = message.text
        chatbot = ChatbotService()
        answer = chatbot.getAnswer(question)
        telegrambot.send_message(message.chat.id, answer)
    else:
        telegrambot.send_message(message.chat.id, 'kirim pesan /start terlebih dahulu untuk mulai chat')

def checkExpiredSession():
    global clients
    for client in clients:
        diffMinutes = abs((client['time'] - datetime.datetime.now()).total_seconds() / 60)
        print('minutes')
        print(diffMinutes)
        if(diffMinutes > 2):
            telegrambot.send_message(client['id'], 'aku tutup ya chatnya :)')
            clients = [i for i in clients if not (i['id'] == client['id'])]

scheduler = BackgroundScheduler(timezone="UTC")
scheduler.add_job(func=checkExpiredSession, trigger="interval", seconds=30)
scheduler.start()
