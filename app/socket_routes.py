from app import socketio, app
from app.services.chatbot_service import ChatbotService
from app.middlewares.auth_middleware import auth_socketio
from flask_socketio import emit, disconnect
from flask import request
import datetime
import time
from apscheduler.schedulers.background import BackgroundScheduler

clients = []

@socketio.on('connect')
@auth_socketio
def connect():
    client = next((item for item in clients if item['id'] == request.sid), False)
    if client == False:
        clients.append({'id':request.sid, 'time': datetime.datetime.now()})
        emit('answer', {'answer': 'Halo :)'})
        emit('answer', {'answer': 'Aku Chat Bot, asisten virtual kamu. ada yang bisa aku bantu. ?'})
        print("connected")
    
@socketio.on('ask-question')
@auth_socketio
def ask(data):
    client = next((item for item in clients if item['id'] == request.sid), False)
    if client != False:
        client['time'] = datetime.datetime.now()
        chatbot = ChatbotService()
        question = data['question']
        answer = chatbot.getAnswer(question)
        emit('answer', {'answer': answer})
    else:
        emit('answer', {'answer': 'silahkan mulai ulang sesi chat'})

@socketio.on('disconnect')
def disconnected():
    global clients
    clients = [i for i in clients if not (i['id'] == request.sid)]


def checkExpiredSession():
    for client in clients:
        diffMinutes = abs((client['time'] - datetime.datetime.now()).total_seconds() / 60)
        print('minutes')
        print(diffMinutes)
        if(diffMinutes > 2):
            with app.test_request_context('/'):
                socketio.emit('answer', {'answer': 'aku tutup ya chatnya :)'}, namespace='/', broadcast=True, to=client['id'])
                time.sleep(5)
                disconnect(sid=client['id'], namespace="/")


scheduler = BackgroundScheduler(timezone="UTC")
scheduler.add_job(func=checkExpiredSession, trigger="interval", seconds=30)
scheduler.start()