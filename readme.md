## How to deploy chatbot

### Steps

1. clone this repo to your server or localserver
1. change enviroment on docker-compose.yml as your enviroment
1. you can change data set QA on app/static/qa-set.xlsx
1. run

```
...
docker-compose build
docker-compose up -d
...
```

## How to use chatbot

### Steps

1. to use web app goto link [{baseurl}:5000](https://localhost:5000) in browser
1. to use telegrambot, if you use my enviroment telegram bot token you can chat to my bot [t.me/nlp04_chatbot](https://t.me/nlp04_chatbot) if not you can use your own bot
