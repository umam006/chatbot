from app import app, socketio, telegrambot
import threading

if __name__ == '__main__':
    bot = threading.Thread(target=telegrambot.infinity_polling, args=())
    bot.start()
    socketio.run(app, host='0.0.0.0', debug= True if app.config.get("APP_ENV") == 'development' else False)